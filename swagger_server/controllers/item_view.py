import connexion
from flask.views import MethodView
from timeit import default_timer
from flask import jsonify


from swagger_server.utils.logging import log as logging
from swagger_server.models.response_model import APIResponse, ErrorResponse
from swagger_server.models.item_model import ItemRequest
from swagger_server.uses_cases.item_use_case import ItemUseCase

class ItemView(MethodView):

    def __init__(self):
        log = logging()
        self.log = log


    def save_item(self):
        """Registro de item

        Enpoint para el registro de item # noqa: E501

        :param body: 
        :type body: dict | bytes

        :rtype: Item
        """
        try:
            star_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Inicio de transacción", "save_item", __name__)

            item_created = None

            if connexion.request.is_json:
                request = ItemRequest(**connexion.request.get_json())
                item_use_case = ItemUseCase()

                item_created = item_use_case.save(request=request)
            api_response = APIResponse(
                success=True,
                data=item_created
            )
            response = api_response.dict()
            end_time = default_timer()
            self.log.info("Funcion: %r - Paquete: %r - Mensaje: Fin de transacción, procesada en: %r milisegundos", "save_item", __name__, round(end_time - star_time)* 1000)
            return jsonify(response)
        except Exception as err:
            end_time = default_timer()
            error_response = APIResponse(
                success=False,
                data=ErrorResponse(
                    error_code=1,
                    error_message=str(err)
                )
            )
            error = error_response.dict()
            return jsonify(error) 

    def get_item_by_id(self, item_id):
        return None
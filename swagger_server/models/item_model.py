from pydantic import BaseModel

class Item(BaseModel):
    id: int

class ItemRequest(BaseModel):
    name:  None